from tkinter import *
from tkinter import ttk, messagebox
import sys, os, re
import webbrowser

if sys.version_info[0] == 2:
    import tkFileDialog as filedialog
else:
    import tkinter.filedialog as filedialog

class DiagCrackError(Exception):
    def __init__(self, error_message='', warning=False):
        if error_message:
            self.error_message = error_message
            if warning:
                messagebox.showwarning("diag cracker message", error_message)
            else:
                messagebox.showerror("diag cracker message", error_message)
    def __str__(self):
        return repr(self.error_message)

class Frame():
    def __init__(self):
        self.business = Business()
        self.root = Tk()
        self.root.geometry('%sx%s' % (450, 200))
        self.root.resizable(0,0)
        self.root.wm_title('DiagCracker')
        self.create_element_on_root()
        self.root.mainloop()

    def create_element_on_root(self):
        self.label_big = Label(self.root, text = 'big log:')
        self.label_big.grid(row=1, column=0, pady=5)
        self.label_small = Label(self.root, text = 'small log:')
        self.label_small.grid(row=2, column=0, pady=5)
        self.label_output = Label(self.root, text = 'output dir:')
        self.label_output.grid(row=3, column=0, pady=5)
        self.label_name = Label(self.root, text = 'file name:')
        self.label_name.grid(row=4, column=0, pady=5)
        self.entry_big = Entry(self.root, bd = 2, width=50)
        # self.entry_big.insert(0, get_json('entry_hil'))
        self.entry_big.grid(row=1, column=1, columnspan=2, sticky='w', pady=5)
        self.entry_small = Entry(self.root, bd = 2, width=50)
        self.entry_small.grid(row=2, column=1, columnspan=2, sticky='w', pady=5)
        self.entry_output = Entry(self.root, bd = 2, width=50)
        self.entry_output.grid(row=3, column=1, columnspan=2, sticky='w', pady=5)
        self.entry_output.insert(0, os.getcwd())
        self.entry_name = Entry(self.root, bd = 2, width=50)
        self.entry_name.grid(row=4, column=1, columnspan=2, sticky='w', pady=5)
        self.button_big = Button(self.root, text = 'browse', command = lambda: self.select_log('big'))
        self.button_big.grid(row=1, column=3, sticky= 'nw', pady=5)
        self.button_small = Button(self.root, text = 'browse', command = lambda: self.select_log('small'))
        self.button_small.grid(row=2, column=3, sticky= 'nw', pady=5)
        self.button_output = Button(self.root, text = 'browse', command = self.select_output_dir)
        self.button_output.grid(row=3, column=3, sticky= 'nw', pady=5)
        self.button_merge = Button(self.root, text = 'merge', command = lambda: self.do_action(self.business.merge_logs, self.entry_big.get(), self.entry_small.get(), self.entry_output.get()))
        self.button_merge.grid(row=5, column=0, sticky= 'nw', pady=5)
        self.button_version = Button(self.root, text = 'version controlled', command = lambda: self.do_action(self.business.make_version_controlled, self.entry_big.get(), self.entry_output.get()))
        self.button_version.grid(row=5, column=1, sticky= 'nw', pady=5)
        self.button_copy = Button(self.root, text = 'copy log', command = self.business.copy_log)
        self.button_copy.grid(row=5, column=2, sticky= 'nw', pady=5)

    def do_action(self, func, *args, **kwargs):
        if self.entry_name.get():
            self.business.output_file_name = self.entry_name.get()
        #self.business.output_log = None TODO check if entrys were changed.
        func(*args, **kwargs)

    def select_log(self, type):
        file_path = filedialog.askopenfilename(filetypes=(("XML files", "*.xml"),("All files", "*.*")))
        #setattr(self.business, '{}_log'.format(type), Business.read_file(file_path))
        getattr(self, 'entry_{}'.format(type)).delete(0, END)
        getattr(self, 'entry_{}'.format(type)).insert(0,file_path)
        if not self.entry_name.get():
            self.entry_name.delete(0, END)
            diag_name =  self.get_diagname_from_path(file_path)
            if diag_name:
                self.entry_name.insert(0, diag_name)

    def get_diagname_from_path(self, file_path):
        match = re.match('(.+_D.+_R\d+).*(\.xml)',file_path.split(os.path.altsep)[-1])
        if match:
            return match.group(1) + match.group(2)

    def select_output_dir(self):
        output_dir = filedialog.askdirectory(initialdir = os.getcwd())
        if output_dir:
            self.entry_output.delete(0, END)
            self.entry_output.insert(0, output_dir)

class Business(object):
    tc_id_regex = re.compile('<.+>(.+)<.+>')
    @staticmethod
    def read_file(path):
        file_text = None
        if path:
            if path.startswith('file:///'):
                path = path[8:]
            with open(path, encoding = "utf8") as file:
                file_text = file.read().split('\n')
            return file_text
        else:
            print('no path was given')

    def __init__(self):
        self.output_file_name = 'output.xml'
        self.output_log = None
        self.last_big_log_path = None
        # self.small_log = None
        output_dir = os.getcwd()
        self.small_index = {}

    def merge_logs(self, big_log_path, small_log_path, output_dir = os.getcwd()):
        if big_log_path and small_log_path and output_dir:
            big_log = Business.read_file(big_log_path)
            small_log = Business.read_file(small_log_path)
            small_indexes = self.gain_small_indexes(small_log)
            self.output_log = self.merge_small_into_big(small_log, big_log, small_indexes)
            # self.output_log = self.make_id_unique(self.output_log)
            self.write_output_log_file(output_dir)
            self.last_big_log_path = big_log_path
            result = messagebox.askquestion('Diag cracker message', 'Merged xml has been created!\n Do you want to open it?', icon = 'question')
            if result == 'yes':
                webbrowser.open(os.path.join(output_dir, self.output_file_name))
        else:
            raise DiagCrackError('Required attributes are missing. Check if all fields are filled')

    def write_output_log_file(self, output_dir):
        with open(os.path.join(output_dir, self.output_file_name),'w+', encoding = "utf8") as file:
            file.write(self.output_log)

    def gain_small_indexes(self, small_log):
        small_indexes = {}
        tc_id_found = False
        last_found = None
        for i, line in enumerate(small_log):
            if tc_id_found == True and '</tc>' in line:
                small_indexes[last_found].append(i)
                tc_id_found = False
            elif tc_id_found == False and '<tc_id>' in line:
                match = self.tc_id_regex.match(line)
                if match:
                    last_found = match.group(1).strip()
                    small_indexes[last_found] = [i]
                    tc_id_found = True
        return small_indexes

    def merge_small_into_big(self, small_log, big_log, small_indexes):
        new_log = big_log[:]
        while small_indexes:
            begin_index = None
            chosen_key = None
            for line_num, line_text in enumerate(new_log):
                if '<tc_id>' in line_text:
                    for key in small_indexes:
                        if key == self.tc_id_regex.match(line_text).group(1).strip():
                            chosen_key = key
                    begin_index = line_num
                elif begin_index and chosen_key and '</tc>' in line_text:
                    first_div_id = self.get_first_id('id="myHeader(\d+)', new_log[begin_index: line_num]) #getting the startnum of the old tc
                    first_assert_id = self.get_first_id('assert_block name="Root" id="(\d+)', new_log[begin_index: line_num][::-1])
                    new_part = small_log[small_indexes[chosen_key][0]: small_indexes[chosen_key][1]]
                    self.replace_div_ids(first_div_id, new_part)
                    self.replace_assert_ids(first_assert_id, new_part)
                    new_log[begin_index: line_num] = new_part  #get new_log's first id numbers and put it in small log.
                    del small_indexes[chosen_key]
                    break
            else:
                raise DiagCrackError('No case matches in the two logs')
        return '\n'.join(new_log)

    def get_first_id(self, pattern, log_lst):
        for item in log_lst:
            match = re.search(pattern, item)
            if match:
                return match.group(1)
        else:
            print('no id number was found for %s' % pattern)

    def replace_div_ids(self, first_div_id, new_part):
        for i, item in enumerate(new_part):
            if 'myHeader' in item and 'ContainerDiv' in item:
                new_part[i] = re.sub('myHeader\d+', 'myHeader{}'.format(first_div_id), item)
                new_part[i] = re.sub('ContainerDiv\d+', 'ContainerDiv{}'.format(first_div_id), new_part[i])
                first_div_id = str(int(first_div_id) + 1)

    def replace_assert_ids(self, first_assert_id, new_part):
        start_id = self.get_first_id('assert_block name="Root" id="(\d+)', reversed(new_part))
        for i, line in reversed(list(enumerate(new_part))):
            if '<diag></diag>' in line:
                break
            elif 'assert' in line and 'id' in line:
                id = self.get_first_id('\sid="(\d+)', [line])
                parent_id = self.get_first_id('\sparent_id="(\d+)', [line])
                if id:
                    new_num = int(id) - int(start_id) + int(first_assert_id)
                    new_part[i] = re.sub('\sid="\d+', ' id="{}'.format(new_num), new_part[i])
                if parent_id and 'Root' not in line:
                    new_num = int(parent_id) - int(start_id) + int(first_assert_id)
                    new_part[i] = re.sub('\sparent_id="\d+', ' parent_id="{}'.format(new_num), new_part[i])

    def make_version_controlled(self, big_log_path, output_dir):
        if big_log_path and output_dir:
            if self.output_log is None or big_log_path != self.last_big_log_path:
                big_log = Business.read_file(big_log_path)
                self.output_log = '\n'.join(big_log)
            self.output_log = re.sub('<not_reviewed>.*</not_reviewed>', '', self.output_log)
            self.output_log = re.sub('<not_official_run>.*</not_official_run>', '', self.output_log)
            self.write_output_log_file(output_dir)
            self.last_big_log_path = big_log_path
            result = messagebox.askquestion('Diag cracker message', 'XML has been made version controlled!\n Do you want to open it?', icon = 'question')
            if result == 'yes':
                webbrowser.open(os.path.join(output_dir, self.output_file_name))
        else:
            raise DiagCrackError('Required attributes are missing. Check if at least big log and output dir fields are given!')

    def copy_log(self):
        if self.output_log:
            chosen_dest_dir = filedialog.askdirectory(initialdir = os.getcwd())
            if chosen_dest_dir:
                self.write_output_log_file(chosen_dest_dir)
                if os.path.isfile(os.path.join(chosen_dest_dir, self.output_file_name)):
                    messagebox.showinfo('Diag cracker message', 'The chosen logfile has been successfuly copied.')
        else:
            raise DiagCrackError('There is nothing to copy. Do a merge or version controlling first!')


if __name__ == '__main__':
    frame = Frame()
